<h1>Inventory Movements Clinical</h1>
<h3>FROM : <?= $from ?></h3>
<h3>TO : <?= $to ?></h3>
<h3>BRANCH : <?= $store ?></h3>
<h3>CATEGORY CLINICAL : <?= $grup_name ?></h3>
<?
$this->pageTitle = 'Inventory Movements Clinical';
$this->widget('CGridViewPlus', array(
    'id' => 'the-table',
    'dataProvider' => $dp,
    'columns' => array(
        array(
            'header' => 'Item Code',
            'name' => 'kode_barang'
        ),
        array(
            'header' => 'Item Name',
            'name' => 'nama_barang'
        ),
        array(
            'header' => 'Beginning Balance',
            'name' => 'BEFORE',
            'value' => function ($data) {
                return format_number_report($data['BEFORE'],0);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'WH (IN)',
            'name' => 'WH',
            'value' => function ($data) {
                return format_number_report($data['WH'],0);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'RELOKASI (IN)',
            'name' => 'RELOKASI',
            'value' => function ($data) {
                return format_number_report($data['RELOKASI'],0);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'PENJUALAN (OUT)',
            'name' => 'PENJUALAN',
            'value' => function ($data) {
                return format_number_report($data['PENJUALAN'],0);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'RELOKASI (OUT)',
            'name' => 'RELOKASIOUT',
            'value' => function ($data) {
                return format_number_report($data['RELOKASIOUT'],0);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'RETUR (OUT)',
            'name' => 'RETUR',
            'value' => function ($data) {
                return format_number_report($data['RETUR'],0);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'Ending Balance',
            'name' => 'AFTER',
            'value' => function ($data) {
                return format_number_report($data['AFTER'],0);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
    )
));