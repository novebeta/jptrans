<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body>
<p>Dear Bapak Ferro,</p>
<p>Berikut data tagihan yang mendekati jatuh tempo.</p>
<?
$this->widget('ext.groupgridview.GroupGridView', array(
    'id' => 'the-table',
    'dataProvider' => $dp,
    'mergeColumns' => array('store','supplier_name'),
    'columns' => array(
        array(
            'header' => 'Branch',
            'name' => 'store'
        ),
        array(
            'header' => 'Supplier',
            'name' => 'supplier_name'
        ),
        array(
            'header' => 'Date',
            'name' => 'tgl'
        ),
        array(
            'header' => 'Customer',
            'name' => 'nama_customer'
        ),
        array(
            'header' => 'No. NWIS',
            'name' => 'doc_ref'
        ),
        array(
            'header' => 'Invoice No.',
            'name' => 'doc_ref_other'
        ),
        array(
            'header' => 'Remain',
            'name' => 'Remain',
            'value' => function ($data) {
                return number_format($data['Remain'], 2);
            },
            'htmlOptions' => array ('style' => 'text-align: right;' ),
            'footerHtmlOptions' => array ('style' => 'text-align: right;' ),
            'footer' => number_format($total_remain,2)
        )
    ),
));
?>
<p>Terima Kasih</p>
</body>
</html>