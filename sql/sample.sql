/*
SQLyog Ultimate v11.3 (64 bit)
MySQL - 5.5.36 : Database - pos
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`pos` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `pos`;

/*Table structure for table `nscc_barang` */

DROP TABLE IF EXISTS `nscc_barang`;

CREATE TABLE `nscc_barang` (
  `barang_id` int(11) NOT NULL AUTO_INCREMENT,
  `kode_barang` varchar(15) NOT NULL,
  `nama_barang` varchar(30) NOT NULL,
  `ket` varchar(50) DEFAULT NULL,
  `grup_id` int(11) NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `price` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`barang_id`),
  UNIQUE KEY `idx_nsc_barang_0` (`kode_barang`,`nama_barang`),
  KEY `idx_nsc_barang` (`grup_id`),
  CONSTRAINT `fk_nsc_barang` FOREIGN KEY (`grup_id`) REFERENCES `nscc_grup` (`grup_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `nscc_barang` */

insert  into `nscc_barang`(`barang_id`,`kode_barang`,`nama_barang`,`ket`,`grup_id`,`active`,`price`) values (1,'AAR','KRIM PAGI AA','Krim pagi anti aging',3,1,20000),(2,'CP-','FACIAL PEELING NON CHEMMICAL','-',1,1,75000);

/*Table structure for table `nscc_beauty` */

DROP TABLE IF EXISTS `nscc_beauty`;

CREATE TABLE `nscc_beauty` (
  `beauty_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_beauty` varchar(20) NOT NULL,
  `gol_id` int(11) NOT NULL,
  PRIMARY KEY (`beauty_id`),
  UNIQUE KEY `idx_nscc_beauty` (`nama_beauty`),
  KEY `idx_nsc_beauty` (`gol_id`),
  CONSTRAINT `fk_nsc_beauty` FOREIGN KEY (`gol_id`) REFERENCES `nscc_gol` (`gol_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `nscc_beauty` */

insert  into `nscc_beauty`(`beauty_id`,`nama_beauty`,`gol_id`) values (1,'NUNING',4);

/*Table structure for table `nscc_customers` */

DROP TABLE IF EXISTS `nscc_customers`;

CREATE TABLE `nscc_customers` (
  `customer_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_customer` varchar(20) NOT NULL,
  `no_customer` varchar(10) NOT NULL,
  `tempat_lahir` varchar(20) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `email` varchar(30) DEFAULT NULL,
  `telp` varchar(25) DEFAULT NULL,
  `alamat` text NOT NULL,
  `kota` varchar(30) NOT NULL,
  `negara` varchar(30) NOT NULL,
  `awal` datetime NOT NULL COMMENT 'AWAL DAFTAR',
  `akhir` datetime NOT NULL COMMENT 'TERAKHIR TRANSAKSI',
  PRIMARY KEY (`customer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `nscc_customers` */

insert  into `nscc_customers`(`customer_id`,`nama_customer`,`no_customer`,`tempat_lahir`,`tgl_lahir`,`email`,`telp`,`alamat`,`kota`,`negara`,`awal`,`akhir`) values (1,'PROBANDUS','P.0001','YOGYAKARTA','1984-05-13','PROBANDUS@GMAIL.COM','0888217132899','JL. Nologaten','YOGYAKARTA','INDONESIA','2014-05-14 14:33:47','2014-05-14 14:33:47');

/*Table structure for table `nscc_gol` */

DROP TABLE IF EXISTS `nscc_gol`;

CREATE TABLE `nscc_gol` (
  `gol_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_gol` varchar(20) NOT NULL,
  PRIMARY KEY (`gol_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `nscc_gol` */

insert  into `nscc_gol`(`gol_id`,`nama_gol`) values (2,'GOL I'),(3,'GOL II'),(4,'GOL III'),(5,'GOL IV'),(6,'GOL V');

/*Table structure for table `nscc_grup` */

DROP TABLE IF EXISTS `nscc_grup`;

CREATE TABLE `nscc_grup` (
  `grup_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_grup` varchar(20) NOT NULL,
  `kategori_id` int(11) NOT NULL,
  PRIMARY KEY (`grup_id`),
  KEY `idx_nsc_grup` (`kategori_id`),
  CONSTRAINT `fk_nsc_grup` FOREIGN KEY (`kategori_id`) REFERENCES `nscc_kategori` (`kategori_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `nscc_grup` */

insert  into `nscc_grup`(`grup_id`,`nama_grup`,`kategori_id`) values (1,'FACIAL',1),(2,'MEDIK',2),(3,'KOSMESTIK',2);

/*Table structure for table `nscc_kategori` */

DROP TABLE IF EXISTS `nscc_kategori`;

CREATE TABLE `nscc_kategori` (
  `kategori_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_kategori` varchar(20) NOT NULL,
  PRIMARY KEY (`kategori_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `nscc_kategori` */

insert  into `nscc_kategori`(`kategori_id`,`nama_kategori`) values (1,'JASA / PERAWATAN'),(2,'OBAT / CREAM');

/*Table structure for table `nscc_price` */

DROP TABLE IF EXISTS `nscc_price`;

CREATE TABLE `nscc_price` (
  `price_id` int(11) NOT NULL AUTO_INCREMENT,
  `value` double NOT NULL DEFAULT '0',
  `barang_id` int(11) NOT NULL,
  `gol_id` int(11) NOT NULL,
  PRIMARY KEY (`price_id`),
  UNIQUE KEY `idx_nsc_price_1` (`barang_id`,`gol_id`),
  KEY `idx_nsc_price` (`barang_id`),
  KEY `idx_nsc_price_0` (`gol_id`),
  CONSTRAINT `fk_nsc_price` FOREIGN KEY (`barang_id`) REFERENCES `nscc_barang` (`barang_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_nsc_price_0` FOREIGN KEY (`gol_id`) REFERENCES `nscc_gol` (`gol_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `nscc_price` */

insert  into `nscc_price`(`price_id`,`value`,`barang_id`,`gol_id`) values (4,5000,2,2);

/*Table structure for table `nscc_salestrans` */

DROP TABLE IF EXISTS `nscc_salestrans`;

CREATE TABLE `nscc_salestrans` (
  `salestrans_id` int(11) NOT NULL AUTO_INCREMENT,
  `tgl` datetime NOT NULL,
  `doc_ref` varchar(20) NOT NULL,
  `tdate` datetime NOT NULL,
  PRIMARY KEY (`salestrans_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `nscc_salestrans` */

/*Table structure for table `nscc_salestrans_details` */

DROP TABLE IF EXISTS `nscc_salestrans_details`;

CREATE TABLE `nscc_salestrans_details` (
  `salestrans_details` int(11) NOT NULL AUTO_INCREMENT,
  `barang_id` int(11) NOT NULL,
  `salestrans_id` int(11) NOT NULL,
  `qty` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`salestrans_details`),
  KEY `idx_nscc_salestrans_details` (`barang_id`),
  KEY `idx_nscc_salestrans_details_0` (`salestrans_id`),
  CONSTRAINT `fk_nscc_salestrans_details` FOREIGN KEY (`barang_id`) REFERENCES `nscc_barang` (`barang_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_nscc_salestrans_details_0` FOREIGN KEY (`salestrans_id`) REFERENCES `nscc_salestrans` (`salestrans_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `nscc_salestrans_details` */

/*Table structure for table `nscc_security_roles` */

DROP TABLE IF EXISTS `nscc_security_roles`;

CREATE TABLE `nscc_security_roles` (
  `security_roles_id` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(20) NOT NULL,
  `ket` varchar(255) DEFAULT NULL,
  `sections` text,
  PRIMARY KEY (`security_roles_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `nscc_security_roles` */

insert  into `nscc_security_roles`(`security_roles_id`,`role`,`ket`,`sections`) values (2,'System Administrator','System Administrator','0000,1000,2000,3000');

/*Table structure for table `nscc_stock_moves` */

DROP TABLE IF EXISTS `nscc_stock_moves`;

CREATE TABLE `nscc_stock_moves` (
  `stock_moves_id` int(11) NOT NULL AUTO_INCREMENT,
  `type_no` int(11) NOT NULL,
  `trans_no` int(11) NOT NULL,
  `tran_date` date NOT NULL,
  `price` double NOT NULL,
  `reference` varchar(40) DEFAULT NULL,
  `qty` double NOT NULL,
  `barang_id` int(11) NOT NULL,
  PRIMARY KEY (`stock_moves_id`),
  KEY `idx_nscc_stock_moves` (`barang_id`),
  CONSTRAINT `fk_nscc_stock_moves` FOREIGN KEY (`barang_id`) REFERENCES `nscc_barang` (`barang_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `nscc_stock_moves` */

/*Table structure for table `nscc_sys_prefs` */

DROP TABLE IF EXISTS `nscc_sys_prefs`;

CREATE TABLE `nscc_sys_prefs` (
  `sys_prefs_id` int(11) NOT NULL AUTO_INCREMENT,
  `name_` varchar(100) NOT NULL,
  `value_` varchar(255) NOT NULL,
  PRIMARY KEY (`sys_prefs_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `nscc_sys_prefs` */

/*Table structure for table `nscc_sys_types` */

DROP TABLE IF EXISTS `nscc_sys_types`;

CREATE TABLE `nscc_sys_types` (
  `sys_types_id` int(11) NOT NULL AUTO_INCREMENT,
  `type_id` smallint(6) NOT NULL,
  `type_no` int(11) NOT NULL,
  `next_reference` varchar(20) NOT NULL,
  PRIMARY KEY (`sys_types_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `nscc_sys_types` */

/*Table structure for table `nscc_users` */

DROP TABLE IF EXISTS `nscc_users`;

CREATE TABLE `nscc_users` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(60) NOT NULL DEFAULT '',
  `password` varchar(100) NOT NULL DEFAULT '',
  `last_visit_date` datetime DEFAULT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `security_roles_id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`) USING BTREE,
  KEY `idx_nsc_users` (`security_roles_id`),
  CONSTRAINT `fk_nsc_users` FOREIGN KEY (`security_roles_id`) REFERENCES `nscc_security_roles` (`security_roles_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `nscc_users` */

insert  into `nscc_users`(`id`,`user_id`,`password`,`last_visit_date`,`active`,`security_roles_id`,`name`) values (1,'admin','7X8ARtt1hFwT2XqxQjzOfo5DKq7kVsxcWsG/vfTf2JBKugQTrJZclTN3J08Nv5qe7UOSTnimU3KTQtzSb9gzGw','2014-05-16 10:24:55',0,2,'ADMINISTRATOR');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
