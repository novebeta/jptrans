jun.Penerimastore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Penerimastore.superclass.constructor.call(this, Ext.apply({
            storeId: 'PenerimaStoreId',
            url: 'Penerima',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'penerima_id'},
                {name: 'nama'},
                {name: 'penerima_grup_id'}
            ]
        }, cfg));
    }
});
jun.rztPenerima = new jun.Penerimastore();
jun.rztPenerimaLib = new jun.Penerimastore();
jun.rztPenerimaCmp = new jun.Penerimastore();
//jun.rztPenerima.load();
