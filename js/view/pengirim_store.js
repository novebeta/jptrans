jun.Pengirimstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Pengirimstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'PengirimStoreId',
            url: 'Pengirim',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'pengirim_id'},
                {name: 'nama'},
                {name: 'prefix_num'},
                {name: 'next_ref'}
            ]
        }, cfg));
    }
});
jun.rztPengirim = new jun.Pengirimstore();
jun.rztPengirimLib = new jun.Pengirimstore();
jun.rztPengirimCmp = new jun.Pengirimstore();
//jun.rztPengirim.load();
