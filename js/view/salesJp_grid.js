jun.SalesJpGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Sales OP",
    id: 'docs-jun.SalesJpGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Doc. Ref',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref',
            width: 100
        },
        {
            header: 'No. Resi',
            sortable: true,
            resizable: true,
            dataIndex: 'no_resi',
            width: 100
        },
        {
            header: 'UP',
            sortable: true,
            resizable: true,
            dataIndex: 'up',
            width: 100
        },
        {
            header: 'Koli',
            sortable: true,
            resizable: true,
            dataIndex: 'koli',
            width: 100
        }
    ],
    initComponent: function () {
        if (jun.rztPengirimCmp.getTotalCount() === 0) {
            jun.rztPengirimCmp.load();
        }
        if (jun.rztPenerimaCmp.getTotalCount() === 0) {
            jun.rztPenerimaCmp.load();
        }
        if (jun.rztBarangJpCmp.getTotalCount() === 0) {
            jun.rztBarangJpCmp.load();
        }
        this.store = jun.rztSalesJp;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Add Sales',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Edit Sales',
                    ref: '../btnEdit'
                }
            ]
        };
        this.store.baseParams = {mode: "grid"};
        this.store.reload();
        this.store.baseParams = {};
        jun.SalesJpGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.SalesJpWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Sales");
            return;
        }
        var idz = selectedz.json.sales_jp_id;
        var form = new jun.SalesJpWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'SalesJp/delete/id/' + record.json.sales_jp_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztSalesJp.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
jun.SalesAdmJpGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Sales",
    id: 'docs-jun.SalesAdmJpGrid',
    iconCls: "silk-grid",
    // viewConfig: {
    //     forceFit: true
    // },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Doc. Ref',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref',
            width: 100
        },
        {
            header: 'No. Resi',
            sortable: true,
            resizable: true,
            dataIndex: 'no_resi',
            width: 100
        },
        {
            header: 'Pengirim',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_pengirim',
            width: 100
        },
        {
            header: 'Penerima',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_penerima',
            width: 100
        },
        {
            header: 'UP',
            sortable: true,
            resizable: true,
            dataIndex: 'up',
            width: 100
        },
        {
            header: 'Tujuan',
            sortable: true,
            resizable: true,
            dataIndex: 'tujuan',
            width: 100
        },
        {
            header: 'WIlayah',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_wilayah',
            width: 100
        },
        {
            header: 'Jalur',
            sortable: true,
            resizable: true,
            dataIndex: 'jalur',
            width: 100
        },
        {
            header: 'Koli',
            sortable: true,
            resizable: true,
            dataIndex: 'koli',
            width: 100
        },
        {
            header: 'Kg',
            sortable: true,
            resizable: true,
            dataIndex: 'kg',
            width: 100
        },
        {
            header: 'Paket Kayu',
            sortable: true,
            resizable: true,
            dataIndex: 'paket_kayu',
            width: 100
        }
    ],
    initComponent: function () {
        if (jun.rztPengirimCmp.getTotalCount() === 0) {
            jun.rztPengirimCmp.load();
        }
        if (jun.rztPenerimaCmp.getTotalCount() === 0) {
            jun.rztPenerimaCmp.load();
        }
        if (jun.rztBarangJpCmp.getTotalCount() === 0) {
            jun.rztBarangJpCmp.load();
        }
        jun.rztSalesAdmJp.on({
            scope: this,
            beforeload: {
                fn: function (a, b) {
                    var tgl = Ext.getCmp('tglsalesjpgridid').getValue();
                    if (tgl == null) {
                        return false;
                    }
                    b.params.tgl = tgl;
                }
            }
        });
        this.store = jun.rztSalesAdmJp;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Add Sales',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Edit Sales',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'label',
                    style: 'margin:5px',
                    text: 'Date :'
                },
                {
                    xtype: 'xdatefield',
                    id: 'tglsalesjpgridid',
                    ref: '../tgl'
                }
            ]
        };
        // this.store.baseParams = {mode: "grid"};
        // this.store.reload();
        // this.store.baseParams = {};
        jun.SalesAdmJpGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.tgl.on('select', this.refreshTgl, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    refreshTgl: function () {
        this.store.reload();
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.SalesAdmJpWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.sales_jp_id;
        var form = new jun.SalesAdmJpWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'SalesJp/delete/id/' + record.json.sales_jp_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztSalesAdmJp.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
