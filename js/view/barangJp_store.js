jun.BarangJpstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.BarangJpstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'BarangJpStoreId',
            url: 'BarangJp',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'barang_jp_id'},
                {name: 'nama'},
                {name: 'kode'},
                {name: 'kat_tujuan_id'},
                {name: 'kg'},
                {name: 'hari'},
                {name: 'koli'},
                {name: 'wilayah_barang_id'}
            ]
        }, cfg));
    }
});
jun.rztBarangJp = new jun.BarangJpstore();
jun.rztBarangJpLib = new jun.BarangJpstore();
jun.rztBarangJpCmp = new jun.BarangJpstore();
//jun.rztBarangJp.load();
