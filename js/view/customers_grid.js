jun.CustomersGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Master Customers",
    id: 'docs-jun.CustomersGrid',
    iconCls: "silk-grid",
    stripeRows: true,
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Customers No.',
            sortable: true,
            resizable: true,
            dataIndex: 'no_customer',
            width: 100,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Customer Name',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_customer',
            width: 100,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Phone',
            sortable: true,
            resizable: true,
            dataIndex: 'telp',
            width: 100,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Address',
            sortable: true,
            resizable: true,
            dataIndex: 'alamat',
            width: 100,
            filter: {xtype: "textfield"}
        }
        /*{
         header: 'Birthplace',
         sortable: true,
         resizable: true,
         dataIndex: 'tempat_lahir',
         width: 100,
         filter: {xtype: "textfield"}
         },
         {
         header: 'Birthday',
         sortable: true,
         resizable: true,
         dataIndex: 'tgl_lahir',
         width: 100,
         renderer: Ext.util.Format.dateRenderer('d M Y'),
         filter: {
         xtype: "xdatefield",
         format: 'd M Y'
         }
         },
         {
         header: 'email',
         sortable: true,
         resizable: true,
         dataIndex: 'email',
         width: 100
         },
         {
         header:'telp',
         sortable:true,
         resizable:true,
         dataIndex:'telp',
         width:100
         },
         {
         header:'alamat',
         sortable:true,
         resizable:true,
         dataIndex:'alamat',
         width:100
         },
         {
         header:'kota',
         sortable:true,
         resizable:true,
         dataIndex:'kota',
         width:100
         },
         {
         header:'negara',
         sortable:true,
         resizable:true,
         dataIndex:'negara',
         width:100
         },
         {
         header:'awal',
         sortable:true,
         resizable:true,
         dataIndex:'awal',
         width:100
         },
         {
         header:'akhir',
         sortable:true,
         resizable:true,
         dataIndex:'akhir',
         width:100
         },
         */
    ],
    initComponent: function () {
        if (jun.rztNegaraCmp.getTotalCount() === 0) {
            jun.rztNegaraCmp.load();
        }
        if (jun.rztProvinsiCmp.getTotalCount() === 0) {
            jun.rztProvinsiCmp.load();
        }
        if (jun.rztKotaCmp.getTotalCount() === 0) {
            jun.rztKotaCmp.load();
        }
        if (jun.rztKecamatanCmp.getTotalCount() === 0) {
            jun.rztKecamatanCmp.load();
        }
        if (jun.rztStatusCustCmp.getTotalCount() === 0) {
            jun.rztStatusCustCmp.load();
        }
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        if (jun.rztInfoCmp.getTotalCount() === 0) {
            jun.rztInfoCmp.load();
        }
        this.store = jun.rztCustomers;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }
            ]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Add Customer',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Edit Customer',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Print CARD',
                    ref: '../btnPrinterCard'
                }
            ]
        };
        this.store.baseParams = {mode: "grid"};
        this.store.reload();
        this.store.baseParams = {};
        jun.CustomersGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnPrinterCard.on('Click', this.onPrintCard, this);
//        this.btnHistory.on('Click', this.onbtnHistoryclick, this);
        this.on('rowdblclick', this.viewForm, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    onPrintCard: function () {
        var selectedz = this.sm.getSelected();
        var type;
        var cust_no;
        var cust_name = "";
        var since;
        var valid;
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a customer");
            return;
        }
        var today = new Date();
        var validDt = today.add(Date.YEAR, VALID_CARD);
        var sinceDt = Date.parseDate(selectedz.json.awal, 'Y-m-d H:i:s');
        var tgl_lahir = Date.parseDate(selectedz.json.tgl_lahir, 'Y-m-d');
        var age = today.getFullYear() - tgl_lahir.getFullYear();
        cust_no = selectedz.json.store + "     " +
        selectedz.json.no_customer.replace(selectedz.json.store, "");
        since = sinceDt.format('m Y');
        valid = validDt.format('m Y');
        if (selectedz.json.sex == 'MALE') {
            type = 'MEN';
        } else {
            type = (age < 0) ? 'TEEN' : 'WOMEN';
        }
        if (selectedz.json.nama_customer.length > 21) {
            arr_name = selectedz.json.nama_customer.split(" ");
            for (index = 0; index < arr_name.length; index++) {
                if ((cust_name.length + arr_name[index].length) > 21) {
                    break;
                }
                cust_name += arr_name[index] + ' ';
            }
        } else {
            cust_name = selectedz.json.nama_customer;
        }
        var msg_ = //'Name : ' + cust_name +
           // '\nCust. No : ' + cust_no +
            'Card Type : ' +
            type + '<br/>Ribbon Colour : ' + (type == 'WOMEN' ? 'BLACK' : 'SILVER');
        //Ext.MessageBox.confirm('PLEASE, PREPARE PRINTER', msg_, this.confirmPrint, this);
        //Ext.Msg.alert('Please, Prepare Card', msg_);
        var strconfirm = Ext.MessageBox.confirm('Print Card','P L E A S E,  P R E P A R E  P R I N T E R' +
        "<br/>==================================<br/>" + msg_);
        if (strconfirm == false) {
            return;
        }
        //findPrinterCard();
        if (notReady()) {
            return;
        }
        Ext.Ajax.request({
            url: 'Customers/SaveLogCard',
            method: 'POST',
            scope: this,
            params: {
                customer_id: selectedz.json.customer_id,
                cardtype: type,
                validcard: validDt.format('Y-m-d'),
                printcarddate: today.format('Y-m-d'),
                locprintcard: STORE
            },
            success: function (f, a) {
                printCard(type, cust_no, cust_name, since, valid);
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    confirmPrint: function (btn) {
        if (btn == 'no') {
            return;
        }
    },
    viewForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a customer");
            return;
        }
        var idz = selectedz.json.customer_id;
        var form = new jun.CustomersWin({modez: 2, id: idz});
        form.formz.getForm().loadRecord(this.record);
        form.show(this);
    },
    onbtnHistoryclick: function () {
        Ext.Ajax.request({
            url: 'customers/createlog',
            method: 'POST',
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                $.ajax({
                    url: response.url,
                    type: "POST",
                    crossDomain: true,
                    data: response,
                    dataType: "json",
                    success: function (result) {
                        alert(JSON.stringify(result));
                    },
                    error: function (xhr, status, error) {
                        alert(status);
                    }
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
        return;
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Customer");
            return;
        }
        var idz = selectedz.json.customer_id;
//        var form = new jun.ReportHistoryCustomers({
//            customer_id: idz,
//            cust_label: "Customer Number : " + selectedz.json.no_customer +
//                "<br>Customer Name : " + selectedz.json.nama_customer +
//                "<br>Birthday : " + selectedz.json.tgl_lahir +
//                "<br>Phone : " + selectedz.json.telp +
//                "<br>Address : " + selectedz.json.alamat
//        });
//        form.show();
        Ext.getCmp("form-ReportViewHistoryCustomers").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportViewHistoryCustomers").getForm().url = "Report/ViewCustomerHistory";
        this.customer_id.setValue(idz);
        this.format.setValue('html');
        this.tglfrom.setValue(this.tgl.getValue().format('Y-m-d'));
        var form = Ext.getCmp('form-ReportViewHistoryCustomers').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.CustomersWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a customer");
            return;
        }
        var idz = selectedz.json.customer_id;
        var form = new jun.CustomersWin({modez: 1, id: idz});
        form.formz.getForm().loadRecord(this.record);
        form.show(this);
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a customer");
            return;
        }
        Ext.Ajax.request({
            url: 'Customers/delete/id/' + record.json.customer_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztCustomers.reload();
                jun.rztCustomersCmp.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
})
