jun.TipeTransClinicalstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.TipeTransClinicalstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'TipeTransClinicalStoreId',
            url: 'TipeTransClinical',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'tipe_clinical_id'},
                {name: 'nama'},
                {name: 'up'},
                {name: 'arus'}
            ]
        }, cfg));
    }
});
jun.rztTipeTransClinical = new jun.TipeTransClinicalstore();
jun.rztClinicalIn = new jun.TipeTransClinicalstore({url:'TipeTransClinical/in'});
jun.rztClinicalOut = new jun.TipeTransClinicalstore({url:'TipeTransClinical/out'});
//jun.rztTipeTransClinical.load();
