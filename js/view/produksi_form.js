jun.ProduksiWin = Ext.extend(Ext.Window, {
    title: 'Production',
    modez: 1,
    width: 925,
    height: 415,
    layout: 'form',
    modal: true,
    padding: 5,
    resizable: !1,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4;padding: 10px',
                id: 'form-Produksi',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'absolute',
                anchor: "100% 100%",
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: "label",
                        text: "Doc. Ref:",
                        x: 5,
                        y: 5
                    },
                    {
                        xtype: 'uctextfield',
                        fieldLabel: 'doc_ref',
                        hideLabel: false,
                        //hidden:true,
                        name: 'doc_ref',
                        id: 'doc_refid',
                        ref: '../doc_ref',
                        maxLength: 50,
                        x: 85,
                        y: 2,
                        height: 20,
                        width: 175,
                        readOnly: true
                    },
                    {
                        xtype: "label",
                        text: "Date:",
                        x: 295,
                        y: 5
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl',
                        fieldLabel: 'tgl',
                        name: 'tgl',
                        id: 'tglid',
                        format: 'd M Y',
                        width: 175,
                        readOnly: true,
                        allowBlank: false,
                        value : DATE_NOW,
                        x: 400,
                        y: 2
                    },
                    {
                        xtype: "label",
                        text: "PL No.:",
                        x: 610,
                        y: 5
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'pl',
                        hideLabel: false,
                        //hidden:true,
                        name: 'pl',
                        id: 'plid',
                        ref: '../pl',
                        maxLength: 50,
                        width: 175,
                        x: 715,
                        y: 2
                    },
                    {
                        xtype: "label",
                        text: "----------- CREATE PRODUCT ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------",
                        x: 5,
                        y: 35
                    },
                    {
                        xtype: "label",
                        text: "Product :",
                        x: 5,
                        y: 65
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'barang_id',
                        store: jun.rztBarangNonJasa,
                        hiddenName: 'barang_id',
                        name: 'barang_id',
                        valueField: 'barang_id',
                        displayField: 'kode_barang',
                        x: 85,
                        y: 62
                    },
                    {
                        xtype: "label",
                        text: "Qty :",
                        x: 295,
                        y: 65
                    },
                    {
                        xtype: 'numericfield',
                        fieldLabel: 'qty',
                        hideLabel: false,
                        //hidden:true,
                        name: 'qty',
                        id: 'qtyid',
                        ref: '../qty',
                        maxLength: 30,
                        value: 1,
                        minValue: 1,
                        x: 400,
                        y: 62
                    },
                    {
                        xtype: "label",
                        text: "----------- FROM PRODUCT -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------",
                        x: 5,
                        y: 95
                    },
                    new jun.ProduksiDetilGrid({
                        x: 5,
                        y: 125,
                        height: 200,
                        frameHeader: !1,
                        header: !1
                    })
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Save & Close',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Close',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.ProduksiWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.on("close", this.onWinClose, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        //this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSaveClose.setVisible(false);
        } else {
            this.btnSaveClose.setVisible(true);
            //this.setDateTime();
        }
    },
    onWinClose: function () {
        jun.rztProduksiDetil.removeAll();
    },
    //setDateTime: function () {
    //    Ext.Ajax.request({
    //        url: 'GetDateTime',
    //        method: 'POST',
    //        scope: this,
    //        success: function (f, a) {
    //            var response = Ext.decode(f.responseText);
    //            this.tgl.setValue(Date.parseDate(response.datetime, 'Y-m-d H:i:s'));
    //        },
    //        failure: function (f, a) {
    //            switch (a.failureType) {
    //                case Ext.form.Action.CLIENT_INVALID:
    //                    Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
    //                    break;
    //                case Ext.form.Action.CONNECT_FAILURE:
    //                    Ext.Msg.alert('Failure', 'Ajax communication failed');
    //                    break;
    //                case Ext.form.Action.SERVER_INVALID:
    //                    Ext.Msg.alert('Failure', a.result.msg);
    //            }
    //        }
    //    });
    //},
    btnDisabled: function (status) {
        //this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        if (jun.rztProduksiDetil.data.length == 0) {
            Ext.Msg.alert('Error', "Item details must set");
            this.btnDisabled(false);
            return;
        }
        var urlz = 'Produksi/create/';
        Ext.getCmp('form-Produksi').getForm().submit({
            url: urlz,
            timeOut: 1000,
            params: {
                detil: Ext.encode(Ext.pluck(
                    jun.rztProduksiDetil.data.items, "data")),
                id: this.id,
                mode: this.modez
            },
            scope: this,
            success: function (f, a) {
                jun.rztProduksi.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-Produksi').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});