jun.BeautyGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Master Beautician",
    id: 'docs-jun.BeautyGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({
        singleSelect: true
    }),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Beautician Name',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_beauty',
            width: 100,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Beautician Class',
            ref: 'colClass',
            sortable: true,
            resizable: true,
            dataIndex: 'gol_id',
            width: 100,
            renderer: jun.renderGol,
            filter: {
                xtype: 'combo',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender: true,
                mode: 'local',
                forceSelection: true,
                store: jun.rztGolCmp,
                hiddenName: 'gol_id',
                valueField: 'gol_id',
                displayField: 'nama_gol'
            }
        },
        {
            header: 'Branch',
            sortable: true,
            resizable: true,
            dataIndex: 'store',
            width: 100,
            filter: {xtype: "textfield"}
        }
    ],
    initComponent: function () {
        jun.rztGolLib.on({
            scope: this,
            load: {
                fn: function (a, b, c) {
                    jun.rztGolCmp.add(b);
                    this.store.baseParams = {
                        mode: "grid"
                    };
                    this.store.reload();
                    this.store.baseParams = {};
                }
            }
        });
        if (jun.rztGolLib.getTotalCount() === 0) {
            jun.rztGolLib.load();
        }
        this.store = jun.rztBeauty;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }
            ]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Add Beautician',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Edit Beautician',
                    ref: '../btnEdit'
                }
            ]
        };
        jun.BeautyGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.BeautyWin({
            modez: 0
        });
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a beautician");
            return;
        }
        var idz = selectedz.json.beauty_id;
        var form = new jun.BeautyWin({
            modez: 1,
            id: idz
        });
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'Beauty/delete/id/' + record.json.beauty_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztBeauty.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
})