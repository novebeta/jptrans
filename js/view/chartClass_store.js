jun.ChartClassstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.ChartClassstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'ChartClassStoreId',
            url: 'ChartClass',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'cid'},
                {name: 'class_name'},
                {name: 'ctype'},
                {name: 'kode'},
                {name: 'up'}
            ]
        }, cfg));
    }
});
jun.rztChartClass = new jun.ChartClassstore();
jun.rztChartClassCmp = new jun.ChartClassstore();
jun.rztChartClassLib = new jun.ChartClassstore();
jun.rztChartClassLib.load();
jun.ChartClasTypesstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.ChartClasTypesstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'ChartClassTypeStoreId',
            url: 'ChartClass/type',
            root: 'results',
            idProperty: "ctype",
            totalProperty: 'total',
            fields: [
                {name: 'ctype'},
                {name: 'class_type_name'}
            ]
        }, cfg));
    }
});
jun.rztChartClassTypeCmp = new jun.ChartClasTypesstore();
jun.rztChartClassTypeLib = new jun.ChartClasTypesstore();
jun.rztChartClassTypeLib.load();
//jun.rztChartClassTypeCmp.load();