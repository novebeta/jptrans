jun.BarangJpGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Tujuan",
    id: 'docs-jun.BarangJpGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Nama Tujuan',
            sortable: true,
            resizable: true,
            dataIndex: 'nama',
            width: 100
        },
        {
            header: 'Kode',
            sortable: true,
            resizable: true,
            dataIndex: 'kode',
            width: 100
        },
        {
            header: 'Harga Udara',
            sortable: true,
            resizable: true,
            dataIndex: 'kg',
            width: 100
        },
        {
            header: 'Durasi',
            sortable: true,
            resizable: true,
            dataIndex: 'hari',
            width: 100
        },
        {
            header: 'Harga Darat',
            sortable: true,
            resizable: true,
            dataIndex: 'koli',
            width: 100
        }
    ],
    initComponent: function () {
        if (jun.rztKatTujuanCmp.getTotalCount() === 0) {
            jun.rztKatTujuanCmp.load();
        }
        if (jun.rztKatTujuanLib.getTotalCount() === 0) {
            jun.rztKatTujuanLib.load();
        }
        if (jun.rztWilayahBarangLib.getTotalCount() === 0) {
            jun.rztWilayahBarangLib.load();
        }
        if (jun.rztWilayahBarangCmp.getTotalCount() === 0) {
            jun.rztWilayahBarangCmp.load();
        }
        this.store = jun.rztBarangJp;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Tambah Tujaun',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Ubah Tujaun',
                    ref: '../btnEdit'
                },
                // {
                //     xtype: 'tbseparator'
                // },
                // {
                //     xtype: 'button',
                //     text: 'Hapus Tujuan',
                //     ref: '../btnDelete'
                // }
            ]
        };
        this.store.baseParams = {mode: "grid"};
        this.store.reload();
        this.store.baseParams = {};
        jun.BarangJpGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        // this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.BarangJpWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.barang_jp_id;
        var form = new jun.BarangJpWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'BarangJp/delete/id/' + record.json.barang_jp_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztBarangJp.reload();
                jun.rztBarangJpCmp.reload();
                jun.rztBarangJpLib.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
