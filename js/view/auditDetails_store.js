jun.AuditDetailsstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.AuditDetailsstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'AuditDetailsStoreId',
            url: 'AuditDetails',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'audit_details'},
                {name: 'barang_id'},
                {name: 'qty'},
                {name: 'beauty_id'},
                {name: 'disc'},
                {name: 'discrp'},
                {name: 'ketpot'},
                {name: 'vat'},
                {name: 'vatrp'},
                {name: 'beauty_tip'},
                {name: 'bruto'},
                {name: 'total'},
                {name: 'total_pot'},
                {name: 'price'},
                {name: 'jasa_dokter'},
                {name: 'audit_id'},
                {name: 'dokter_id'}
            ]
        }, cfg));
        this.on('add', this.refreshData, this);
        this.on('update', this.refreshData, this);
        this.on('remove', this.refreshData, this);
    },
    refreshData: function (a) {
        var subtotal = this.sum("total");
        var vatrp = this.sum("vatrp");
        var discrp = parseFloat(Ext.getCmp("discrpid").getValue());
        Ext.getCmp("subtotalid").setValue(subtotal);
        Ext.getCmp("vatid").setValue(vatrp);
        var total = subtotal - vatrp - discrp;
        Ext.getCmp("totalid").setValue(total);
    }
});
jun.rztAuditDetails = new jun.AuditDetailsstore();
//jun.rztAuditDetails.load();
