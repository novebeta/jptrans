jun.PenerimaGrupstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.PenerimaGrupstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'PenerimaGrupStoreId',
            url: 'PenerimaGrup',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'penerima_grup_id'},
                {name: 'nama'}
            ]
        }, cfg));
    }
});
jun.rztPenerimaGrup = new jun.PenerimaGrupstore();
jun.rztPenerimaGrupLib = new jun.PenerimaGrupstore();
jun.rztPenerimaGrupCmp = new jun.PenerimaGrupstore();
//jun.rztPenerimaGrup.load();
