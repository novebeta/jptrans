jun.KatTujuanstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.KatTujuanstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'KatTujuanStoreId',
            url: 'KatTujuan',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'kat_tujuan_id'},
                {name: 'nama'},
            ]
        }, cfg));
    }
});
jun.rztKatTujuan = new jun.KatTujuanstore();
jun.rztKatTujuanLib = new jun.KatTujuanstore();
jun.rztKatTujuanCmp = new jun.KatTujuanstore();
//jun.rztKatTujuan.load();
