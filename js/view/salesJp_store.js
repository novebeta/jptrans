jun.SalesJpstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.SalesJpstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'SalesJpStoreId',
            url: 'SalesJp',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name:'sales_jp_id'},
                {name:'doc_ref'},
                {name:'no_resi'},
                {name:'tgl'},
                {name:'up'},
                {name:'kg'},
                {name:'koli'},
                {name:'paket_kayu'},
                {name:'tarif_paket_kayu'},
                {name:'tarif'},
                {name:'total_biaya'},
                {name:'nominal_paket_kayu'},
                {name:'pengirim_id'},
                {name:'penerima_id'},
                {name:'tdate'},
                {name:'user_id'},
                {name:'barang_jp_id'},
                {name:'disc'},
                {name:'disc_rp'},
                {name:'total'},
                {name:'total_kirim'},
                {name:'jalur'},
                {name:'wilayah_barang_id'},
                {name:'nama_pengirim'},
                {name:'nama_penerima'},
                {name:'tujuan'},
                {name:'kode_tujuan'},
                {name:'nama_wilayah'}
            ]
        }, cfg));
    }
});
jun.rztSalesJp = new jun.SalesJpstore();
jun.rztSalesAdmJp = new jun.SalesJpstore();
//jun.rztSalesJp.load();
