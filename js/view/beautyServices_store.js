jun.BeautyServicesstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.BeautyServicesstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'BeautyServicesStoreId',
            url: 'BeautyServices',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'nscc_beauty_services_id'},
                {name: 'total'},
                {name: 'beauty_id'},
                {name: 'salestrans_details'},
                {name: 'final'}
            ]
        }, cfg));
    }
});
jun.rztBeautyServices = new jun.BeautyServicesstore();
//jun.rztBeautyServices.load();
