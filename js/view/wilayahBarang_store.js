jun.WilayahBarangstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.WilayahBarangstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'WilayahBarangStoreId',
            url: 'WilayahBarang',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'wilayah_barang_id'},
                {name: 'nama_wilayah'}
            ]
        }, cfg));
    }
});
jun.rztWilayahBarang = new jun.WilayahBarangstore();
jun.rztWilayahBarangLib = new jun.WilayahBarangstore();
jun.rztWilayahBarangCmp = new jun.WilayahBarangstore();
//jun.rztWilayahBarang.load();
