<?php
class AreaController extends GxController
{
    public function actionCreate()
    {
        if (!Yii::app()->request->isAjaxRequest)
            $this->redirect(url('/'));
        if (isset($_POST) && !empty($_POST)) {
            $status = false;
            $msg = "Stored data failed.";
            $detils = CJSON::decode($_POST['detil']);
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $is_new = $_POST['mode'] == 0;
                $model = $is_new ? new Area : $this->loadModel($_POST['area_id'], 'Area');
                if (!$is_new && ($model == null)) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'Area')) . "Fatal error, record not found.");
                }
                if (!$is_new) {
                   
                    StoreArea::model()->deleteAll('area_id = :area_id',
                        array(':area_id' => $model->area_id));
                }
                foreach ($_POST as $k => $v) {
                    if ($k == 'detil') continue;
                    if (is_angka($v)) $v = get_number($v);
                    $_POST['Area'][$k] = $v;
                }
                
                $model->attributes = $_POST['Area'];
                if (!$model->save())
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Area')) . CHtml::errorSummary($model));
                
                foreach ($detils as $detil) {
                    $item_details = new StoreArea;
                    
                    $item_details->store = $detil['store'];
                    $item_details->area_id = $model->area_id;
                    if (!$item_details->save())
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'StoreArea'
                            . 'tails')) . CHtml::errorSummary($item_details));
                }
                $transaction->commit();
                $msg = t('save.success', 'app');
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
   
    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = '<b>Area</b> was successfully removed.';
            $status = true;
            try {
                $this->loadModel($id, 'Area')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = 'Removing <b>Area</b> failed.<br>'.$ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }
    
    public function actionIndex()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        
        $model = Area::model()->findAll($criteria);
        $total = Area::model()->count($criteria);
        $this->renderJson($model, $total);
    }
    
     public function actionStore()
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition("area_id = :area_id");
        $criteria->params = array(':area_id' => $_POST['area_id']);
        $model = StoreArea::model()->findAll($criteria);
        $total = StoreArea::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}