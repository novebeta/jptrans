<?php
class BeautyServicesController extends GxController
{
    public function actionCreate()
    {
        $model = new BeautyServices;
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['BeautyServices'][$k] = $v;
            }
            $model->attributes = $_POST['BeautyServices'];
            $msg = "Data gagal disimpan.";
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->nscc_beauty_services_id;
            } else {
                $msg .= " " . implode(", ", $model->getErrors());
                $status = false;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'BeautyServices');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['BeautyServices'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['BeautyServices'];
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->nscc_beauty_services_id;
            } else {
                $msg .= " " . implode(", ", $model->getErrors());
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->nscc_beauty_services_id));
            }
        }
    }
    public function actionIndex()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        $model = BeautyServices::model()->findAll($criteria);
        $total = BeautyServices::model()->count($criteria);
        $this->renderJson($model, $total);
    }
    public function actionCheckFinal()
    {
        if (Yii::app()->request->isPostRequest) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $store = $_POST['store'];
            $arr = U::get_date_service_no_final($from, $to,$store);
            echo CJSON::encode(array(
                'success' => count(array_column($arr,'tgl')) == 0,
                'msg' => "You have not set final service : <br>" . implode("<br>", array_column($arr,'tgl'))));
            Yii::app()->end();
        } else
            throw new CHttpException(403,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }
}