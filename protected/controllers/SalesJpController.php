<?php
class SalesJpController extends GxController
{
    public function actionCreate()
    {
        if (!Yii::app()->request->isAjaxRequest) {
            $this->redirect(url('/'));
        }
        if (isset($_POST) && !empty($_POST)) {
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $number = SalesJp::model()->findAllByAttributes(array('no_resi' => $_POST['no_resi']));
                if (count($number) > 0) {
                    throw new Exception("No. Resi sudah terinput dalam sistem. Duplikat " . count($number));
                }
                $model = new SalesJp;
                $ref = new Reference();
                $docref = $ref->get_next_reference(PENJUALAN);
                foreach ($_POST as $k => $v) {
                    if (is_angka($v)) $v = get_number($v);
                    $_POST['SalesJp'][$k] = $v;
                }
                $_POST['SalesJp']['doc_ref'] = $docref;
                $model->attributes = $_POST['SalesJp'];
                if (!$model->save()) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'Sales')) . CHtml::errorSummary($model));
                }
                $ref->save(PENJUALAN, $model->sales_jp_id, $docref);
                $transaction->commit();
                $msg = "Data berhasil di simpan dengan id " . $model->sales_jp_id;
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                'success' => $status,
                'id' => $docref,
                'msg' => $msg
            ));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        /** @var $model SalesJp */
        $model = $this->loadModel($id, 'SalesJp');
        $model->setIsNewRecord(false);
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['SalesJp'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['SalesJp'];
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->sales_jp_id;
            } else {
                $msg .= " " . CHtml::errorSummary($model);
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->sales_jp_id));
            }
        }
    }
    public function actionUpdateAdm($id)
    {
        $number = SalesJp::model()->findAllByAttributes(array('no_resi' => $_POST['no_resi']));
        if (count($number) > 1) {
            throw new Exception("No. Resi sudah terinput dalam sistem. Duplikat " . count($number));
        }
        /** @var $model SalesJp */
        $model = $this->loadModel($id, 'SalesJp');
        $model->setIsNewRecord(false);
        if (isset($_POST) && !empty($_POST)) {
            $transaction = Yii::app()->db->beginTransaction();
            try {
                foreach ($_POST as $k => $v) {
                    if (is_angka($v)) $v = get_number($v);
                    $_POST['SalesJp'][$k] = $v;
                }
//                $msg = "Data gagal disimpan";
                $model->attributes = $_POST['SalesJp'];
                $barangjp = BarangJp::model()->findByPk($model->barang_jp_id);
                if ($barangjp == null) {
                    throw new Exception("Master tujuan tidak ditemukan");
                }
                $model->wilayah_barang_id = $barangjp->wilayah_barang_id;
                if (!$model->save()) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'Sales')) . CHtml::errorSummary($model));
                }
                if ($model->jalur == "DARAT") {
                    $model->tarif = $barangjp->koli;
                    $model->total_biaya = $model->tarif * $model->koli;
                } else {
                    $model->tarif = $barangjp->kg;
                    $model->total_biaya = $model->tarif * $model->kg;
                }
                if ($model->disc > 0.00) {
                    $model->disc_rp = $model->total_biaya * ($model->disc / 100);
                    $model->total_kirim = $model->total_biaya - $model->disc_rp;
                }
                if ($model->paket_kayu > 0) {
                    $model->tarif_paket_kayu = SysPrefs::get_val('Biaya paket kayu');
                    $model->nominal_paket_kayu = $model->paket_kayu * $model->tarif_paket_kayu;
                }
                $model->total = $model->nominal_paket_kayu + $model->total_kirim;
                if (!$model->save()) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'Sales')) . CHtml::errorSummary($model));
                }
                $transaction->commit();
                $msg = "Data berhasil di simpan dengan id " . $model->sales_jp_id;
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg
            ));
            Yii::app()->end();
        }
    }
    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'SalesJp')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }
    public function actionIndex()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        $param = array();
        if (isset($_POST['tgl'])) {
            $criteria->addCondition('tgl = :tgl');
            $param[':tgl'] = $_POST['tgl'];
        }
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        $criteria->params = $param;
        $model = SalesJpView::model()->findAll($criteria);
        $total = SalesJpView::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}