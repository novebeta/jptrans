<?php

Yii::import('application.models._base.BaseJual');
class Jual extends BaseJual
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public static function save_jual($barang_id, $price, $cost, $store = STOREID)
    {
        $comm = Yii::app()->db->createCommand(
            "REPLACE INTO nscc_jual (price, cost, store, up, barang_id)
                VALUES (:price, :cost, :store, 1, :barang_id)"
        );
        return $comm->execute(array(
            ':price' => $price,
            ':cost' => $cost,
            ':store' => $store,
            ':barang_id' => $barang_id
        ));
    }
    public function beforeValidate()
    {
        if ($this->jual_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->jual_id = $uuid;
        }
        return parent::beforeValidate();
    }
}