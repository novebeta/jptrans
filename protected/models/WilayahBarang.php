<?php
Yii::import('application.models._base.BaseWilayahBarang');

class WilayahBarang extends BaseWilayahBarang
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->wilayah_barang_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->wilayah_barang_id = $uuid;
        }
        return parent::beforeValidate();
    }
}