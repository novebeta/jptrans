<?php

Yii::import('application.models._base.BaseKecamatan');

class Kecamatan extends BaseKecamatan
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate()
    {
        if ($this->kecamatan_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->kecamatan_id = $uuid;
        }
        return parent::beforeValidate();
    }
}