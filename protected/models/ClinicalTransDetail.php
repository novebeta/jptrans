<?php

Yii::import('application.models._base.BaseClinicalTransDetail');

class ClinicalTransDetail extends BaseClinicalTransDetail
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate()
    {
        if ($this->clinical_trans_detail_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->clinical_trans_detail_id = $uuid;
        }
        return parent::beforeValidate();
    }
}