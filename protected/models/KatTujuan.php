<?php
Yii::import('application.models._base.BaseKatTujuan');

class KatTujuan extends BaseKatTujuan
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->kat_tujuan_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->kat_tujuan_id = $uuid;
        }
        return parent::beforeValidate();
    }
}