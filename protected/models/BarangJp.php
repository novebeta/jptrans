<?php
Yii::import('application.models._base.BaseBarangJp');

class BarangJp extends BaseBarangJp
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->barang_jp_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->barang_jp_id = $uuid;
        }
        return parent::beforeValidate();
    }
}