<?php

/**
 * This is the model base class for the table "{{wilayah_barang}}".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "WilayahBarang".
 *
 * Columns in table "{{wilayah_barang}}" available as properties of the model,
 * followed by relations of table "{{wilayah_barang}}" available as properties of the model.
 *
 * @property string $wilayah_barang_id
 * @property string $nama_wilayah
 *
 * @property BarangJp[] $barangJps
 */
abstract class BaseWilayahBarang extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return '{{wilayah_barang}}';
	}

	public static function representingColumn() {
		return 'nama_wilayah';
	}

	public function rules() {
		return array(
			array('wilayah_barang_id, nama_wilayah', 'required'),
			array('wilayah_barang_id, nama_wilayah', 'length', 'max'=>36),
			array('wilayah_barang_id, nama_wilayah', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
			'barangJps' => array(self::HAS_MANY, 'BarangJp', 'wilayah_barang_id'),
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'wilayah_barang_id' => Yii::t('app', 'Wilayah Barang'),
			'nama_wilayah' => Yii::t('app', 'Nama Wilayah'),
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('wilayah_barang_id', $this->wilayah_barang_id, true);
		$criteria->compare('nama_wilayah', $this->nama_wilayah, true);

		return new CActiveDataProvider(get_class($this), array(
			'criteria' => $criteria,
		));
	}
}