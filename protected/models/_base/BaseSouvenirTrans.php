<?php

/**
 * This is the model base class for the table "{{souvenir_trans}}".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "SouvenirTrans".
 *
 * Columns in table "{{souvenir_trans}}" available as properties of the model,
 * followed by relations of table "{{souvenir_trans}}" available as properties of the model.
 *
 * @property string $souvenir_trans_id
 * @property integer $type_no
 * @property string $trans_no
 * @property string $ref
 * @property string $tgl
 * @property integer $qty
 * @property string $tdate
 * @property string $id_user
 * @property integer $up
 * @property string $souvenir_id
 * @property string $store
 *
 * @property Souvenir $souvenir
 */
abstract class BaseSouvenirTrans extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return '{{souvenir_trans}}';
	}

	public static function representingColumn() {
		return 'trans_no';
	}

	public function rules() {
		return array(
			array('souvenir_trans_id, type_no, trans_no, tgl, tdate, id_user, souvenir_id, store', 'required'),
			array('type_no, qty, up', 'numerical', 'integerOnly'=>true),
			array('souvenir_trans_id, souvenir_id', 'length', 'max'=>36),
			array('trans_no, ref, id_user', 'length', 'max'=>50),
			array('store', 'length', 'max'=>20),
			array('ref, qty, up', 'default', 'setOnEmpty' => true, 'value' => null),
			array('souvenir_trans_id, type_no, trans_no, ref, tgl, qty, tdate, id_user, up, souvenir_id, store', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
			'souvenir' => array(self::BELONGS_TO, 'Souvenir', 'souvenir_id'),
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'souvenir_trans_id' => Yii::t('app', 'Souvenir Trans'),
			'type_no' => Yii::t('app', 'Type No'),
			'trans_no' => Yii::t('app', 'Trans No'),
			'ref' => Yii::t('app', 'Ref'),
			'tgl' => Yii::t('app', 'Tgl'),
			'qty' => Yii::t('app', 'Qty'),
			'tdate' => Yii::t('app', 'Tdate'),
			'id_user' => Yii::t('app', 'Id User'),
			'up' => Yii::t('app', 'Up'),
			'souvenir_id' => Yii::t('app', 'Souvenir'),
			'store' => Yii::t('app', 'Store'),
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('souvenir_trans_id', $this->souvenir_trans_id, true);
		$criteria->compare('type_no', $this->type_no);
		$criteria->compare('trans_no', $this->trans_no, true);
		$criteria->compare('ref', $this->ref, true);
		$criteria->compare('tgl', $this->tgl, true);
		$criteria->compare('qty', $this->qty);
		$criteria->compare('tdate', $this->tdate, true);
		$criteria->compare('id_user', $this->id_user, true);
		$criteria->compare('up', $this->up);
		$criteria->compare('souvenir_id', $this->souvenir_id);
		$criteria->compare('store', $this->store, true);

		return new CActiveDataProvider(get_class($this), array(
			'criteria' => $criteria,
		));
	}
}