<?php

Yii::import('application.models._base.BaseTender');
class Tender extends BaseTender
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->tender_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->tender_id = $uuid;
        }
        if ($this->tdate == null) {
            $this->tdate = new CDbExpression('NOW()');
        }
        if ($this->store == null) {
            $this->store = STOREID;
        }
        if ($this->user_id == null) {
            $this->user_id = Yii::app()->user->getId();
        }
        return parent::beforeValidate();
    }
    public static function is_exist($tgl,$store = STOREID)
    {
        $tender = Tender::model()->find('DATE(tgl) = :tgl AND store = :store',
            array(':tgl' => $tgl, ':store' => $store));
        return $tender != null;
    }
    public function save_printz(){
        $printz = new Printz;
//        $printz->prntz_id = U::generate_primary_key(RPRINTZ);
        $printz->store = $this->store;
        $printz->date_ = $this->tgl;
        $user = Users::model()->findByPk(Yii::app()->getUser()->id);
        $printz->employee = $user->name;
        $sales = Salestrans::get_disc_tax_bayar_change_all($this->tgl, $this->store);
//        $purchase = TransferItem::get_total_all_cash($this->tgl, $this->store);
        $tendered = $sales->bayar;
        $started = U::get_balance_before_for_bank_account($this->tgl, Bank::get_bank_cash_id($this->store), $this->store);
        $added = Kas::get_cash_in($this->tgl, $this->store);
        $removed = abs(Kas::get_cash_out($this->tgl, $this->store));
        $collected = $tendered - $sales->kembali + $started + $added - $removed;
        $counted = $this->total;
        $printz->sales = Salestrans::get_total_sales($this->tgl, $this->store);
        $printz->return_sales = abs(Salestrans::get_total_returnsales($this->tgl, $this->store));
        $printz->tax_sales = $sales->vat;
        $printz->disc_sales = $sales->totalpot;
        $printz->tendered = $tendered;
        $printz->change_ = $sales->kembali;
        $printz->started = $started;
        $printz->added = $added;
        $printz->removed = $removed;
        $printz->counted = $counted;
        $printz->status_ = $collected - $counted;
        $printz->tender_id = $this->tender_id;
        if (!$printz->save())
            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'PrintZ')) . CHtml::errorSummary($printz));
        foreach ($this->tenderDetails as $td) {
            $ztotal = 0;
            $printz_details = new PrintzDetails;
            if($td->bank->is_bank_cash($this->store)){
                $ztotal += $started;
                $printz_details->started = $started;
                $printz_details->cash = 1;
            }else{
                $printz_details->started = 0;
                $printz_details->cash = 0;
            }
            $printz_details->nama_bank = $td->bank->nama_bank;
            $printz_details->added = $td->get_added();
            $ztotal += $printz_details->added;
            $printz_details->collected = $td->get_collected_by_bank();
            $ztotal += $printz_details->collected;
            $printz_details->removed = abs($td->get_removed());
            $ztotal -= $printz_details->removed;
            $printz_details->counted = $td->amount;
            $printz_details->status_ = $td->amount - $ztotal;
            $printz_details->prntz_id = $printz->prntz_id;
            if (!$printz_details->save())
                throw new Exception(t('save.model.fail', 'app', array('{model}' => 'PrintzDetails')) . CHtml::errorSummary($printz_details));
        }
    }
}