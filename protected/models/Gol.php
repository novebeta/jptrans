<?php

Yii::import('application.models._base.BaseGol');

class Gol extends BaseGol
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate()
    {
        if ($this->gol_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->gol_id = $uuid;
        }
        return parent::beforeValidate();
    }
}