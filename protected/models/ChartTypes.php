<?php
Yii::import('application.models._base.BaseChartTypes');
class ChartTypes extends BaseChartTypes
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->id = $uuid;
        }
        return parent::beforeValidate();
    }
    public static function get_chart_types_by_class($ctype)
    {
        $comm = Yii::app()->db->createCommand("SELECT
        nct.id,nct.`kode`,nct.`name`,nct.parent,nct.hide
        FROM nscc_chart_types AS nct
#        INNER JOIN nscc_chart_class AS ncc ON nct.class_id = ncc.cid
        WHERE nct.class_id = :ctype AND (LENGTH(parent) = 0 OR parent is null)
		UNION ALL
		SELECT '' AS id,ncm.account_code AS kode,
		       ncm.account_name AS `name`,ncm.kategori AS parent,0 AS hide
        FROM nscc_chart_master AS ncm
		WHERE ncm.kategori = :ctype
        ORDER BY kode");
        return $comm->queryAll(true, array(':ctype' => $ctype));
    }
    public static function get_child($id)
    {
        $comm = Yii::app()->db->createCommand("SELECT * FROM
        nscc_chart_types AS nct WHERE nct.parent = :id
        ORDER BY nct.kode");
        return $comm->queryAll(true, array(':id' => $id));
    }
    public static function get_arr_all_child($type_id)
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition('kategori = :kategori');
//        $criteria->order = 'seq';
	    $criteria->order  = 'account_code';
        $criteria->params = array(':kategori' => $type_id);
        /** @var ChartTypeMaster[] $chart */
        $chart = ChartTypeMaster::model()->findAll($criteria);
        $arr = array();
        foreach ($chart as $key => $item) {
            $arr[] = array(
                'id' => $item->id,
                'account_code' => $item->account_code,
                'account_name' => $item->account_name,
                'node_type' => ($item->saldo_normal != '') ? 'M' : 'T',
                'node_name' => ($item->saldo_normal != '')  ? 'COA' : 'Chart Types',
//                'hide' => $item->hide,
//                'parent' => $item->parent,
//                'class_id' => $item->class_id,
//                'seq' => $item->seq,
                'saldo_normal' => ($item->saldo_normal != '') ? $item->saldo_normal : null,
                'children' => ($item->saldo_normal != '')  ? null : self::get_arr_all_child($item->id),
                'leaf' => ($item->saldo_normal != '')
            );
        }
//        $criteria = new CDbCriteria();
//        $criteria->addCondition('kategori = :kategori');
//        $criteria->order = 'account_code';
//        $criteria->params = array(':kategori' => $type_id);
//	    $criteria->order  = 'account_code';
//        $chartMaster = ChartMaster::model()->findAll($criteria);
//        foreach ($chartMaster as $key => $item) {
//            $arr[] = array(
//                'id' => $item->account_code,
//                'account_code' => $item->account_code,
//                'account_name' => $item->account_name,
//                'node_type' => 'M',
//                'node_name' => 'COA',
//                'saldo_normal' => $item->saldo_normal,
//                'kategori' => $item->kategori,
//                'tipe' => $item->tipe,
//                'leaf' => true
//            );
//        }
        return $arr;
    }
}