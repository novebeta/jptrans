<?php
Yii::import('application.models._base.BasePengirim');

class Pengirim extends BasePengirim
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->pengirim_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->pengirim_id = $uuid;
        }
        return parent::beforeValidate();
    }
}