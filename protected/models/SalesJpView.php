<?php
Yii::import('application.models._base.BaseSalesJpView');

class SalesJpView extends BaseSalesJpView
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function primaryKey()
    {
        return "sales_jp_id";
    }
}