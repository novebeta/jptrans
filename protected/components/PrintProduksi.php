<?php
/**
 * Created by PhpStorm.
 * User: LENOVO
 * Date: 23/03/2015
 * Time: 15:43
 */

class PrintProduksi extends BasePrint{
    /* @var $s Produksi */
    private $s;
    function PrintProduksi($s)
    {
//        $s = new Kas;
        $this->s = $s;
    }
    public function buildTxt()
    {
        $newLine = "\r\n";
        $raw = parent::setCenter(SysPrefs::get_val('receipt_header0'));
        $raw .= $newLine;
        $raw .= parent::setCenter(SysPrefs::get_val('receipt_header1'));
        $raw .= $newLine;
        $raw .= parent::setCenter(SysPrefs::get_val('receipt_header2'));
        $raw .= $newLine;
        $raw .= parent::setCenter(SysPrefs::get_val('receipt_header3'));
        $raw .= $newLine;
        $raw .= parent::setCenter(SysPrefs::get_val('receipt_header4'));
        $raw .= $newLine;
        $raw .= parent::setCenter(SysPrefs::get_val('receipt_header5'));
        $raw .= $newLine;
        $raw .= parent::fillWithChar("=");
        $raw .= $newLine;
        $raw .= "P  R  O  D  U  C  T  I  O  N";
        $raw .= $newLine;
        $raw .= parent::fillWithChar("-");
        $raw .= $newLine;
        $raw .= parent::addHeaderSales("Branch", $this->s->store);
        $raw .= $newLine;
        $raw .= parent::addHeaderSales("Doc. Ref", $this->s->doc_ref);
        $raw .= $newLine;
        $raw .= parent::addHeaderSales("PL No.", $this->s->pl);
        $raw .= $newLine;
        $raw .= parent::addHeaderSales("Date", date('m/d/Y', strtotime($this->s->tgl)));
        $raw .= $newLine;
        $raw .= parent::addHeaderSales("Print Date", date('m/d/Y H:i:s'));
        $user = Users::model()->findByPk(Yii::app()->getUser()->id);
        $raw .= $newLine;
        $raw .= parent::addHeaderSales("Employee", $user->name);
        $raw .= $newLine;
        $raw .= parent::fillWithChar("-");
        $raw .= $newLine;
        $raw .= "CREATE PRODUCT";
        $raw .= $newLine;
        $raw .= parent::addLeftRight($this->s->barang->kode_barang, $this->s->qty);
        $raw .= $newLine;
        $raw .= "FROM PRODUCT";
        $raw .= $newLine;
        foreach($this->s->produksiDetils as $item){
            $raw .= parent::addLeftRight($item->barang->kode_barang, $item->qty);
            $raw .= $newLine;
        }
        $raw .= parent::fillWithChar("=");
        $raw .= $newLine;
        $raw .= parent::setCenter(SysPrefs::get_val('receipt_footer0'));
        $raw .= $newLine;
        $raw .= parent::setCenter(SysPrefs::get_val('receipt_footer1'));
        $raw .= $newLine;
        $raw .= parent::setCenter(SysPrefs::get_val('receipt_footer2'));
        $raw .= $newLine;
        $raw .= parent::setCenter(SysPrefs::get_val('receipt_footer3'));
        $raw .= $newLine;
        $raw .= parent::setCenter(SysPrefs::get_val('receipt_footer4'));
        $raw .= $newLine;
        $raw .= parent::setCenter(SysPrefs::get_val('receipt_footer5'));
        $raw .= $newLine;
        U::save_file(ReportPath.$this->s->doc_ref.'.txt',$raw);
        return $raw;
    }
    public function buildreturnTxt()
    {
        $newLine = "\r\n";
        $raw = parent::setCenter(SysPrefs::get_val('receipt_header0'));
        $raw .= $newLine;
        $raw .= parent::setCenter(SysPrefs::get_val('receipt_header1'));
        $raw .= $newLine;
        $raw .= parent::setCenter(SysPrefs::get_val('receipt_header2'));
        $raw .= $newLine;
        $raw .= parent::setCenter(SysPrefs::get_val('receipt_header3'));
        $raw .= $newLine;
        $raw .= parent::setCenter(SysPrefs::get_val('receipt_header4'));
        $raw .= $newLine;
        $raw .= parent::setCenter(SysPrefs::get_val('receipt_header5'));
        $raw .= $newLine;
        $raw .= parent::fillWithChar("=");
        $raw .= $newLine;
        $raw .= "P  R  O  D  U  C  T  I  O  N    R  E  T  U  R  N";
        $raw .= $newLine;
        $raw .= parent::fillWithChar("-");
        $raw .= $newLine;
        $raw .= parent::addHeaderSales("Branch", $this->s->store);
        $raw .= $newLine;
        $raw .= parent::addHeaderSales("Doc. Ref", $this->s->doc_ref);
        $raw .= $newLine;
        $raw .= parent::addHeaderSales("Production Doc. Ref", $this->s->doc_ref_produksi);
        $raw .= $newLine;
        $raw .= parent::addHeaderSales("Date", date('m/d/Y', strtotime($this->s->tgl)));
        $raw .= $newLine;
        $raw .= parent::addHeaderSales("Print Date", date('m/d/Y H:i:s'));
        $user = Users::model()->findByPk(Yii::app()->getUser()->id);
        $raw .= $newLine;
        $raw .= parent::addHeaderSales("Employee", $user->name);
        $raw .= $newLine;
        $raw .= parent::fillWithChar("-");
        $raw .= $newLine;
        $raw .= "CREATE PRODUCT";
        $raw .= $newLine;
        $raw .= parent::addLeftRight($this->s->barang->kode_barang, $this->s->qty);
        $raw .= $newLine;
        $raw .= "FROM PRODUCT";
        $raw .= $newLine;
        foreach($this->s->produksiDetils as $item){
            $raw .= parent::addLeftRight($item->barang->kode_barang, $item->qty);
            $raw .= $newLine;
        }
        $raw .= parent::fillWithChar("=");
        $raw .= $newLine;
        $raw .= parent::setCenter(SysPrefs::get_val('receipt_footer0'));
        $raw .= $newLine;
        $raw .= parent::setCenter(SysPrefs::get_val('receipt_footer1'));
        $raw .= $newLine;
        $raw .= parent::setCenter(SysPrefs::get_val('receipt_footer2'));
        $raw .= $newLine;
        $raw .= parent::setCenter(SysPrefs::get_val('receipt_footer3'));
        $raw .= $newLine;
        $raw .= parent::setCenter(SysPrefs::get_val('receipt_footer4'));
        $raw .= $newLine;
        $raw .= parent::setCenter(SysPrefs::get_val('receipt_footer5'));
        $raw .= $newLine;
        U::save_file(ReportPath.$this->s->doc_ref.'.txt',$raw);
        return $raw;
    }
}